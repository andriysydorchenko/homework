
let userNumber = +prompt("Введите свое число");

while(!Number.isInteger(userNumber)){
    userNumber = +prompt("Введите целое число");
}

let i = 5;

if (i > userNumber) {
    console.log("Sorry, no numbers");
}
else {
    while (i <= userNumber){
        console.log(i);
        i += 5;
    }
}


console.log("Prime Number");

let m = +prompt("Введите меньшее число");
let n = +prompt("Введите большие число");

while ( !( n > m && Number.isInteger(n) && Number.isInteger(m) ) ){
    m = +prompt("Введите меньшее число");
    n = +prompt("Введите большие число");
}
loop1:
    for (let i = m; i < n; i++){
        for (let k = 2; k < i; k++){
            if(i % k === 0) {
                continue loop1;
            }
        }
        console.log(i);
    }