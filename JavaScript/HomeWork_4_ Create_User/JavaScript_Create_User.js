
function CreateNewUser() {
    this._name = prompt("Enter your name");
    this._lastName = prompt("Enter your last name");

    this.getLogin = function() {
        let firstLetter = this._name[0];
        return firstLetter.toLowerCase() + this._lastName.toLowerCase();
    };

    this.setFirstName = function(firstName) {
        this._name = firstName;
    };

    this.setLastName = function(lastName) {
        this._lastName = lastName;
    };
}

let newUser = new CreateNewUser();


console.log(newUser);
console.log(newUser.getLogin());


// CHANGE FIRST NAME AND LAST NAME
newUser.setFirstName(prompt("Enter your new name"));
newUser.setLastName(prompt("Enter your new last name"));
console.log(newUser);
console.log(newUser.getLogin());