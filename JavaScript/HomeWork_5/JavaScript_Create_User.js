
function CreateNewUser() {
    this._name = prompt("Enter your name");
    this._lastName = prompt("Enter your last name");
    this.birthday = prompt("Enter your date Birthday in format dd.mm.yyyy");

    this.getLogin = function() {
        let firstLetter = this._name[0];
        return firstLetter.toLowerCase() + this._lastName.toLowerCase();
    };

    this.setFirstName = function() {
        this._name = prompt("Enter your new name");
    };

    this.setLastName = function() {
        this._lastName = prompt("Enter your new last name");
    };

    this.getAge = function() {
        const nowDate = new Date();
        const year = this.birthday.slice(6);
        const month = this.birthday.slice(3,5) - 1;
        const day = this.birthday.slice(0,2);
        const dateBirthday = new Date(year, month, day);
        const difDate = new Date(nowDate - dateBirthday);
        const age = difDate.getFullYear() - 1970;

        // Старый способ подсчета возраста
        // let age = nowDate.getFullYear() - year;
        // if (month > nowDate.getMonth() || (month === nowDate.getMonth() && day > nowDate.getDate())) {
        //     age--;
        // }

        return age;
    };

    this.getPassword = function() {
        let firstLetter = this._name[0];
        return firstLetter.toUpperCase() + this._lastName.toLowerCase() + this.birthday.slice(6) ;
    };

}

let newUser = new CreateNewUser();




console.log(newUser);
console.log(`User login - ${newUser.getLogin()}`);
console.log(`User age - ${newUser.getAge()}`);
console.log(`User password - ${newUser.getPassword()}`);



// CHANGE FIRST NAME AND LAST NAME
// newUser.setFirstName();
// newUser.setLastName();
// console.log(newUser);
// console.log(newUser.getLogin());