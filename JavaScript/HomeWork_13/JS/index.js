
let themeNow = localStorage.getItem("themeNow");

$("#btn-normal-theme").click(function () {
    localStorage.setItem("themeNow", "normalTheme");
    toggleTheme("normalTheme");
});
$("#btn-green-theme").click(function () {
    localStorage.setItem("themeNow", "greenTheme");
    toggleTheme("greenTheme");
});
function toggleTheme(themeNow) {
     if (themeNow === "greenTheme") {
        $("body").addClass("green-theme");
        $(".navbar-list").addClass("green-theme").find(".navbar-link").addClass("green-theme");
        $(".left-navbar-link").addClass("green-theme");
        $(".main-content-text").addClass("green-theme");
        $(".footer").addClass("green-theme");
    } else{
        $("body").removeClass("green-theme");
        $(".navbar-list").removeClass("green-theme").find(".navbar-link").removeClass("green-theme");
        $(".left-navbar-link").removeClass("green-theme");
        $(".main-content-text").removeClass("green-theme");
        $(".footer").removeClass("green-theme");
    }
}

document.addEventListener("DOMContentLoaded", function (){
    toggleTheme(themeNow);
});