import React from 'react';
import Input from "../Input/Input";
import {Button} from "../Button"

const FormEditBook = ({inputs, handleSubmit, handleChangeBookForm}) => {
    const formInputs = inputs.map( input => {
        return (
            <td>
                <Input type ={input.type} name={input.name} value={input.value} handleChange={handleChangeBookForm} id={input.id}/>
            </td>
        )
    });

    return (
        <div>
            <form action="" id="update-book-form">
                <tr>
                    {formInputs}
                    <td>
                        <Button type="submit" styleType="primary" text="Update" handleClick={(event) => handleSubmit(event, inputs[0].id)}/>
                    </td>
                    <td/>
                </tr>
            </form>
        </div>
    );
};

export default FormEditBook;
