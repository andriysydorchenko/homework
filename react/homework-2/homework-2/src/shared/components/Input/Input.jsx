import React, { useState } from "react";

const Input = (props) => {

    const [validate, setValidate] = useState(true);

    const handleBlur = ({ target }) => {
        const { value } = target;
        const result = !!value;
        setValidate(result);
    }

    const { className, errorText, label, handleChange, ...attr } = props;
    const inputLabel = label ? <label htmlFor="title">{label}</label> : "";
    const fullClassName = `form-control mb-2 ${className || ""}`;
    const error = !validate ? <div className='alert alert-danger p-1' role='alert'>{errorText}</div> : '' ;
    return(
        <div className="form-group">
            {inputLabel}
            <input {...attr} onChange={handleChange} onBlur={handleBlur} className={fullClassName} />
            {error}
        </div>
    )

}

Input.defaultProps = {
    type: "text",
    errorText: "Поле обязательно для заполнения!"
};
export default Input;