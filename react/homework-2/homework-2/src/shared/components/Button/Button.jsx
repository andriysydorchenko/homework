import React from 'react';

const btnTypes = {
    outline: 'btn-outline',
    primary: 'btn-primary',
    light: 'btn-light',
    info: 'btn-info btn-sm',
    delete: 'btn-danger btn-sm btn-delete'
}

export const Button =({type, styleType, text, handleClick})=>{
    return(
        <button type={type} className={`btn ${btnTypes[styleType]}`} onClick={handleClick}>{text}</button>
    );
}