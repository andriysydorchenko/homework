import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';

// if you using css deleting scss import, and uncomment this import
// import '../../../shared/styles/scss/style.css';
import {Header} from "../../../client/example/components/Header";
import {Books} from "../../../client/example/components/Books/components/Books";


export const App = ()=> {
        return (
            <div className="container mt-4">
                <Header/>
                <Books/>
            </div>
        );

};
