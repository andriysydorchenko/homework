import React, {useState} from "react";
import {Input} from "../../../../../../shared/components/Input";
import {Button} from "../../../../../../shared/components/Button";

const AddBookFrom = ({inputs, button, handleChange, handleSubmit})=>{

    const onSubmit = (event)=>{
        event.preventDefault();
        handleSubmit();
    }

    const formInputs = inputs.map( input => <Input key={input.name} {...input} handleChange={handleChange}/>);
    return(
        <div className="col-lg-4">
            <form  onSubmit = {onSubmit} id="add-book-form">
                {formInputs}
                <Button {...button}/>
            </form>
        </div>
    )
}

export default AddBookFrom;