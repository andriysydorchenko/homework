import React from "react";
import {Book} from "../../../../../../shared/components/Book";
import FormEditBook from "../../../../../../shared/components/Form-Edit-Book";

const BookList = ({books, handleDelete, handleEdit, handleSubmitUpdateBook, handleChangeBookForm})=>{
    const bookList = books.map(book => {
        if (book.isEdit) {
            return <FormEditBook key={book.isbn} inputs={book.inputs} handleSubmit={handleSubmitUpdateBook} handleChangeBookForm={handleChangeBookForm}/>
        }
        return <Book key={book.isbn} {...book} handleDelete={handleDelete} handleEdit={handleEdit}/>
    })
    return(
        <>
            <h3 id="book-count" className="book-count mt-5">Всего книг: {books.length}</h3>
            <table className="table table-striped mt-2">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>ISBN#</th>
                    <th>Title</th>
                </tr>
                </thead>
                <tbody id="book-list">
                    {bookList}
                </tbody>

            </table>
        </>
    )
}

export default BookList;