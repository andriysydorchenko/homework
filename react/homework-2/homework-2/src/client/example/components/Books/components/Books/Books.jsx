import React, {Component} from "react";
import {AddBookForm} from "../Add-Book-Form";
import {BookList} from "../BookList";

class Books extends Component {
    state= {
        addBookForm :{
            inputs: [
                {
                    type: "text",
                    name: "title",
                    label: "Title",
                    value: ""
                },
                {
                    type: "text",
                    name: "author",
                    label: "Author",
                    value: ""
                },
                {
                    type: "text",
                    name: "isbn",
                    label: "ISBN#",
                    value: ""
                }
            ],
            button: {
                type: "submit",
                styleType: "primary",
                text: "Add Book"
            }
        },
        books: [
            {
                title: "Малазанская книга Падших",
                author: "Стивен Эриксон",
                isbn: "978-3-16-148410-0",
                isEdit: false,
                input: []
            }
        ],
    }
    updateBookList = (bookList, newBook) =>{
        const newBooks = bookList.map(book => {
            const newBook = {...book}
            return newBook;
        });
        newBooks.push(newBook);
        return newBooks;
    }

    clearInputs = (inputs) =>{
        const newInputs = inputs.map( input =>{
            const newInput = {...input};
            newInput.value = "";
            return newInput
        });
        return newInputs;
    }

    handleSubmitCreateBook = ()=>{
        this.setState(state => {
            // console.log(state);
            const {addBookForm, books} = state;
            // console.log(addBookForm);
            const newBook = {};
            addBookForm.inputs.forEach(input => {
                const {name, value} = input;
                newBook[name] = value;
            })

            const newInputs = this.clearInputs(addBookForm.inputs);
            // const newBooks = this.updateBookList(books, newBook);
            const newBooks = [...books, newBook];

            const newAddBookForm = Object.assign(addBookForm, {inputs: newInputs});

            return {
                addBookForm: newAddBookForm,
                books: newBooks
            }
        })
    }

    updateInputs =(inputs, name, value)=>{
        const newInputs = inputs.map( input =>{
            const newInput = {...input};
            if (newInput.name === name) {
                newInput.value = value;
            }
            return newInput
        })

        return newInputs;
    }

    handleChange = (event)=>{
        event.preventDefault();
        const {name, value}= event.target;
        this.setState( ({addBookForm}) =>{
            console.log(addBookForm);
            const newInputs = this.updateInputs(addBookForm.inputs, name, value);
            const newAddBookForm = Object.assign(addBookForm, {inputs: newInputs});

            return {
                    addBookForm: newAddBookForm
            }
        })
    }




    handleSubmitUpdateBook = (event, id) =>{
        event.preventDefault();
        this.setState(({books}) =>{
            const newBooks = books.map(book =>{
                const newBook = {...book};
                if (newBook.isbn === id) {
                    const {inputs} = newBook
                    inputs.forEach(input => {
                        newBook[input.name] = input.value;
                    })
                }
                newBook.isEdit = false;
                return newBook;
            })
            return {
                books: newBooks
            }
        })


    }



    handleDelete = (isbn)=>{
        const {books} = this.state;
        const index = books.findIndex(book => book.isbn===isbn);
        const newBookList = [...books.slice(0, index), ...books.slice(index+1)];
        this.setState({
            books: newBookList
        })
    }

    handleEdit = (isbn)=> {
        this.setState( ({books}) =>{
            const newBooks = books.map(book => {
                const newBook = {...book};
                if (newBook.isbn === isbn) {
                    newBook.isEdit = true;
                    newBook.inputs = [
                        {
                            type: "text",
                            name: "title",
                            value: newBook.title,
                            id: newBook.isbn
                        },
                        {
                            type: "text",
                            name: "author",
                            value: newBook.author,
                            id: newBook.isbn
                        },
                        {
                            type: "text",
                            name: "isbn",
                            value: newBook.isbn,
                            id: newBook.isbn
                        }
                    ]
                }
                return newBook
            })
            return {
                books: newBooks
            }
        })
    }
    handleChangeBookForm = (event)=>{
        event.preventDefault();
        const {name, value, id}= event.target;
        this.setState(state => {
            const indexBook = state.books.findIndex( book => book.isbn = id);
            const {inputs} = state.books[indexBook];
            const newInputs = inputs.map( input => {
                const newInput = {...input};
                if (newInput.name === name) {
                    newInput.value = value;
                }
                return newInput;
            })
            const newBooks = state.books.map(book => {
                const newBook = {...book};
                if (newBook.isbn === id) {
                    newBook.inputs = newInputs;
                }
                return newBook;
            })

            return {
                books: newBooks
            }
        })
    }

    render(){
        return(
            <div>
                <AddBookForm inputs={this.state.addBookForm.inputs} button={this.state.addBookForm.button} handleChange={this.handleChange} handleSubmit = {this.handleSubmitCreateBook}/>
                <BookList books={this.state.books} handleDelete = {this.handleDelete} handleEdit={this.handleEdit} handleSubmitUpdateBook={this.handleSubmitUpdateBook} handleChangeBookForm={this.handleChangeBookForm}/>
            </div>
        )
    }
}
export default Books;