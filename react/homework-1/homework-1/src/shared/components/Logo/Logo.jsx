import React from "react";

import './Logo.css';

export const Logo = ({alt="logo", src, text1, text2}) => {
    const logoImg = (src) ? <img src={src} alt={alt} className='logo-img'/> : "logo";
    const logoText1 = (text1) ? <span className='logo-text text-uppercase font-weight-bold'>{text1}</span> : "";
    const logoText2 = (text2) ? <span className='logo-text text-uppercase font-weight-light'>{text2}</span> : "";
    return(
        <a className='logo' href='/'>
            {logoImg}
            {logoText1}
            {logoText2}
        </a>
    );
}