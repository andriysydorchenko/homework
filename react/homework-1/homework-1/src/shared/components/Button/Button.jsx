import React from 'react';
import "./Button.css"
const btnTypes = {
    outline: "btn-outline",
    primary: "btn-primary",
    light: "btn-light",
    dropdown: "btn-dropdown",
    filter: "btn-filter"

}
export const Button =(props)=>{
    const {type, href, text}=props;
    return(
        <a href={href} className={`btn ${btnTypes[type]}`}>{text}</a>
    );
}