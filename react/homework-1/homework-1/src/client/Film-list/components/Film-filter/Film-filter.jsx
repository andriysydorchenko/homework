import React from "react";
import "./Film-filter.css";
import {Button} from "../../../../shared/components/Button";

const FilmFilter = (props)=>{
    const {buttons} = props;
    const filterButtons = buttons.map( button => <Button {...button}/>)
    return(
        <div className="filter d-flex justify-content-between align-items-center">
            <div className="filter-buttons">
                {filterButtons}
            </div>
            <div className="filter-grid d-flex justify-content-between align-items-center">
                <div className="filter-grid-square">
                    <div className="square-item"></div>
                    <div className="square-item"></div>
                    <div className="square-item"></div>
                    <div className="square-item"></div>
                </div>
                <div className="filter-grid-row">
                    <div className="row-item"></div>
                    <div className="row-item"></div>
                </div>
            </div>
        </div>
    )
}
export default FilmFilter;