import React from 'react';
import "./Film-card.css"
import Tarzan from "../../../../assets/images/films/tarzan.png";

const FilmCard = (props) =>{
    const {title,genres,rating, src} = props;
    return(
        <div className="film">
            <img src={src} alt="" className="film-img"/>
            <div className='d-flex justify-content-around align-items-center'>
                <div className="film-short-info">
                    <h3 className="film-title">{title}</h3>
                    <div className="film-genres">
                        {genres.join(", ")}
                    </div>
                </div>
                <div className="film-rating">{rating}</div>
            </div>

        </div>
    )
}
export default FilmCard;