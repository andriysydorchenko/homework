import React, {Component} from 'react';
import './Film-list.css';
import {FilmCard} from "../Film-card/";
import {FilmFilter} from "../Film-filter";
import {Button} from "../../../../shared/components/Button";
import HeaderBG from "../../../../assets/images/header-bg-img.png";

class FilmList extends Component {
    render(){
        const {filmCards, filmFilter, filmBanner} = this.props;
        const filmList = filmCards.map(film => {
            return (<FilmCard {...film}/>)
        })
        console.log(filmBanner.src);
        // const styleBanner: {
        //     backgroundImage: ...filmBanner.src
        // }

        return(
                <div className="film-list d-flex flex-column justify-content-between">
                    <FilmFilter {...filmFilter}/>
                    <div className="films d-flex justify-content-between flex-wrap">
                        {filmList}
                        {filmList}
                        {filmList}
                        <div className="film-list-banner" >
                            <div>
                                <div className="banner-title">{filmBanner.title}</div>
                                <Button {...filmBanner.button}/>
                            </div>
                        </div>
                    </div>
                </div>


        )
    }
}
export default FilmList