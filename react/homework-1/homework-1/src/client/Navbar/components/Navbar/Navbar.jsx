import React from "react";

import "./Navbar.css"
import {Logo} from "../../../../shared/components/Logo";
import {SearchForm} from "../../../../shared/components/Search-form/";
import {Button} from "../../../../shared/components/Button";

export const Navbar = (props)=>{
    const {alt=null, src=null, text1, text2} =props.logo;
    return(
        <div className="navbar d-flex justify-content-between">
            <Logo alt={alt} src={src} text1={text1} text2={text2}/>
            <div className="d-flex justify-content-between align-items-center">
                <SearchForm/>
                {/*<Button type={props.btnSingIn.type} href={} text={}/>*/}
                <Button {...props.btnSingIn}/>
                <Button {...props.btnSingUp}/>
            </div>

        </div>
    )
} ;