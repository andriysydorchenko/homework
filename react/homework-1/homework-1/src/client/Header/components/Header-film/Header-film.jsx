import React from "react";
import "./Header-film.css";
import {Button} from "../../../../shared/components/Button";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import DropDownMenu from '../../../../shared/components/Drop-down-menu';


export const HeaderFilm = (props)=>{
    const {title, genres, duration, rating, buttons} = props;
    const filmGenres = genres.map( genre => <span className="genre">{genre}</span>);
    function getStars(rating){
        const starsRating = Math.round(rating);
        let stars = [];
        for (let i = 1; i <= 5; i++){
            if (i <= starsRating) {
                stars.push(<span className="checked"><FontAwesomeIcon icon={faStar} /></span>);
            } else stars.push(<span className=""><FontAwesomeIcon icon={faStar} /></span>);
        }
        return stars;
    }
    const filmButtons = buttons.map(btn => <Button {...btn}/>);

    return(
        <div className="header-film d-flex justify-content-between align-items-end">
            <div className="header-film-info">
                <h2 className="header-film-title">{title}</h2>
                <div className="header-film-genres">
                    {filmGenres}
                    <span className="genre">|</span>
                    <span className="genre">{duration}</span>
                </div>
                <div className="header-film-rating">
                    {getStars(rating)}
                    <span className="header-film-rating-count">{rating}</span>
                </div>
            </div>
            <div className="header-film-functional">
                {filmButtons}
                <DropDownMenu/>
            </div>
        </div>

    )
}