import React from "react";
import {Navbar} from "../../../Navbar/components/Navbar";
import {HeaderFilm} from "../Header-film";
import "./Header.css";
import HeaderBG from "../../../../assets/images/header-bg-img.png"
const headerStyle = {
    backgroundImage: `url(${HeaderBG})`
}

export const Header =  (props)=> {
    return(
        <div className="header d-flex flex-column justify-content-between" style={headerStyle}>
            <Navbar {...props.navbar}/>
            <HeaderFilm {...props.headerFilm}/>
        </div>

    )
};