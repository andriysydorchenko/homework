import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../../shared/styles/scss/style.scss';


// if you using css deleting scss import, and uncomment this import
// import '../../../shared/styles/scss/style.css';
import '../../../assets/fonts/HelveticaNeueCyr/stylesheet.css'


import {Header} from "../../../client/Header/components/Header";
import {FilmList} from "../../../client/Film-list/components/Film-list/";


// import images films
import Fantastic from "../../../assets/images/films/fantastic_beasts_and_where_to_find_them.png";
import AssAssin from "../../../assets/images/films/creed.png";
import Now from "../../../assets/images/films/nowyouseeme.png";
import Tarzan from "../../../assets/images/films/tarzan.png";
import Banner from "../../../assets/images/banner.png";

const propsPage = {
    header:{
        navbar : {
            logo: {
                alt: "logo",
                text1: "MOVIE",
                text2: "rise"
            },
            btnSingIn: {
                type: "light",
                href: "",
                text: "Sing in"
            },
            btnSingUp: {
                type: "primary",
                href: "",
                text: "Sing Up"
            }
        },
        headerFilm :{
            title: "The Jungle Book",
            genres: ['Adventure','Drama','Family','Fantasy'],
            duration: "1h 46m",
            rating: 4.8,
            buttons: [
                {
                    type: "primary",
                    href: "",
                    text: "Watch Now"
                },
                {
                    type: "outline",
                    href: "",
                    text: "View Info"
                },
                {
                    type: "light",
                    href: "",
                    text: "+ Favorites"
                }
            ]
        }
    },
    filmList:{
        filmFilter: {
            buttons: [
                {
                    type: 'filter',
                    href: "",
                    text: "Trending"
                },
                {
                    type: 'filter',
                    href: "",
                    text: 'Top Rated'
                },
                {
                    type: 'filter',
                    href: "",
                    text: 'New Arrivals'
                },
                {
                    type: 'filter',
                    href: "",
                    text: 'Trailers'
                },
                {
                    type: 'filter',
                    href: "",
                    text: 'Coming soon'
                },
                {
                    type: 'filter',
                    href: "",
                    text: 'Genre'
                },

            ]
        },
        filmCards: [
            {
                title: "Fantastic Beasts ",
                genres: ['Adventure', 'Family', 'Fantasy'],
                rating: 4.7,
                src: Fantastic
             },
            {
                title: "AssAssin’s Creed",
                genres: ['Action', 'Adventure', 'Fantasy'],
                rating: 4.2,
                src: AssAssin
            },
            {
                title: "Now you see me 2",
                genres: ['Action', 'Adventure', 'Comedy'],
                rating: 4.4,
                src: Now
            },
            {
                title: "The Legend of Tarzan",
                genres: ['Action', 'Adventure', 'Drama'],
                rating: 4.3,
                src: Tarzan
            }
        ],
        filmBanner: {
            title: "Receive information on the latest hit movies straight to your inbox",
            button: {
                type: "primary",
                href: "",
                text: "Subscribe!"
            },
            src: {Banner}
        }
    }

}

export const App = () => {
    return (
        <div className="container">
            <Header {...propsPage.header}/>
            <FilmList {...propsPage.filmList}/>
        </div>
    );
};
