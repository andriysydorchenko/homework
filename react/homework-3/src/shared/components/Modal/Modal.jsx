import React, {useContext} from 'react';
import Button from "../Button/Button";
import style from "./Modal.module.css";
import {PageContext} from "../../../store/PageContext";


export const Modal = ({children}) => {
    const {context: {isOpenCartModal}, dispatch} = useContext(PageContext);

    const styleDispalay = {
        display: isOpenCartModal ? "block": "none"
    };

    const closeCartModal = () => {
        dispatch({type: "CLOSE-CART-MODAL"});
    }
return (
    <div  className={style.modal} style={styleDispalay}>
        <Button type='close' text = {"X"} handleClick={closeCartModal}/>
        {children}
        <Button type='secondary' text = "Close" handleClick={closeCartModal}/>
    </div>
    )
}