import React from 'react';
import logo from './logo.svg';
import './App.css';
import { ProductApp } from './client/ProductApp/components/ProductApp';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Modal} from "./shared/components/Modal";
import Navbar from "./client/Navbar/components/Navbar";
import {PageContextProvider} from "./store/PageContext";
import {ProductCart} from "./client/ProductApp/components/ProductCart";

function App() {
  return (
      <PageContextProvider>
            <div className="App">
              <Navbar/>
              <Modal>
                  <ProductCart/>
              </Modal>
                <ProductApp/>
            </div>
      </PageContextProvider>
  );
}

export default App;
