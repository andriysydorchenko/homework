import React, {useContext} from 'react'
import {PageContext} from "../../../../store/PageContext";

export const ProductCart = () => {
    const {context: {cart}, dispatch} = useContext(PageContext);
    let totalPrice = 0;

    const cartElements = cart.map((element, index) =>{
        const price = element.count*element.product.price;
        totalPrice += price;
        return ( <li className="row mb-2 d-flex">
            <span  className="col-20">{element.product.title} </span>
            <div className="col d-flex justify-content-center">
                <button className="btn btn-outline-dark mr-1" onClick={() => dispatch({type: "INCREMENT-COUNT", idx: index})}>+</button>
                <span className="mr-1">Count: {element.count} </span>
                <button className="btn btn-outline-dark mr-1" onClick={() => dispatch({type:"DECREMENT-COUNT", idx: index})}>-</button>
                <span className="mr-1">Price: ${price.toFixed(2)} </span>
                <button className="btn btn-outline-danger mr-1" onClick={() => dispatch({type: "REMOVE-FROM-CART", idx: index})} >Delete</button>
            </div>
        </li>)
    }

    )
    
    return (
        <ul className="d-flex flex-column">
            <h2>Выбранное</h2>
            {cartElements}
            <hr/>
            <h3>Total price: ${totalPrice.toFixed(2)}</h3>
        </ul>
    )
}