import React, {useContext} from 'react'
import {productsListProps} from "./productListProps";
import {PageContext} from "../../../../store/PageContext"

export const ProductsList = () => {
    const {context, dispatch} = useContext(PageContext);
    // console.log(dispatch);

    const productsElements = context.productsListProps.map(product =>{
       const {title, price, src} = product;
       return (
        < div className = "card-block" >
            <h4 className = "card-title" > {title} </h4>
            <p className="card-text">Price: ${price.toFixed(2)}</p>
            <img className="img-fluid" src={src} alt={title}/>
            <a href="#"  className="add-to-cart btn btn-primary" onClick={() => dispatch({type: "ADD-TO-CART", elem: product})}>Add to cart</a>
        </div>
       )
    })

    return (
        <ul className="d-flex">
            <h2>Products:</h2>
            {productsElements}
        </ul>
    )
}