import React, {useContext} from 'react';
import Button from "../../../../shared/components/Button/Button";
import {PageContext} from "../../../../store/PageContext";

const Navbar = () => {
    const {dispatch} = useContext(PageContext);
    const openCartModal = ()=> {
        dispatch({type: "OPEN-CART-MODAL"});
    }
    const clearCart = () => {
        dispatch({type: "ClEAR-CART"});
    }
    return (
        <div className="d-flex justify-content-center">
            <Button type="warning" text= "Open cart" handleClick={openCartModal}/>
            <Button type="danger" text= "Clear cart" handleClick={clearCart}/>
        </div>
    );
};

export default Navbar;
