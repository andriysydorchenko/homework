const deleteCartElement = (state, index)=>{
    const newCartList = [...state.cart];
    newCartList.splice(index, 1)
    window.localStorage.setItem("cart", JSON.stringify(newCartList));
    return {...state,cart: newCartList}
}

const changeCount = (state, index, increment = true) => {
    const newCartList = [...state.cart];
    if (increment) {
        newCartList[index] = {
            product: newCartList[index].product,
            count: newCartList[index].count + 1
        };
        window.localStorage.setItem("cart", JSON.stringify(newCartList));
        return {...state, cart: newCartList}
    }else {
        if (newCartList[index].count - 1 === 0)
            return deleteCartElement(state, index);
        else{
            newCartList[index] = {
                product: newCartList[index].product,
                count: newCartList[index].count - 1
            };
            window.localStorage.setItem("cart", JSON.stringify(newCartList));

            return {...state, cart: newCartList}
        }
    }
}

const addToCart = (state, cartElement) =>{
    const index = state.cart.findIndex( ({product: {id}}) => id === cartElement.id);
    if (index === -1) {
        const newCartElement = {
            product: cartElement,
            count: 1
        }
        const newCartList  = [...state.cart, newCartElement];
        const newState = {...state, cart: newCartList}
        window.localStorage.setItem("cart", JSON.stringify(newCartList));
        return newState
    } else return changeCount(state, index);
}

export const changeCartList = (state, action) => {
    switch (action.type) {
        case "ADD-TO-CART":
            return addToCart(state, action.elem)

        case "REMOVE-FROM-CART":
            return deleteCartElement(state, action.idx)

        case "ClEAR-CART":
            return {...state, cart: []};

        case "INCREMENT-COUNT":
            return changeCount(state, action.idx)

        case "DECREMENT-COUNT":
            return changeCount(state, action.idx, false)

        case "OPEN-CART-MODAL":
            return {...state, isOpenCartModal: true}

        case "CLOSE-CART-MODAL":
            return {...state, isOpenCartModal: false}

        default:
            return state;
    }
}