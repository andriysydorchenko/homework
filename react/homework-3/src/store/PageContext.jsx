import React, {useReducer} from 'react';
import {initialState} from "./intialState";
import {changeCartList} from "./reducers/reducer";


const PageContext = React.createContext(initialState);

const PageContextProvider = ({children}) =>{

    const [context, dispatch] = useReducer(changeCartList, initialState);

    return(
        <PageContext.Provider value={{context, dispatch}}>
            {children}
        </PageContext.Provider>
    )
}

export {PageContext, PageContextProvider};