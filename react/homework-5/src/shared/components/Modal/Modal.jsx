import React from 'react';
import Button from "../Button/Button";
import style from "./Modal.module.css";
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {createCloseCartModal} from "../../../store/creators/createCloseCartModal";


export const Modal = ({children}) => {
    const isOpenCartModal = useSelector(state => state.isOpenCartModal, shallowEqual);
    const dispatch = useDispatch();

    const styleDisplay = {
        display: isOpenCartModal ? "block": "none"
    };

return (
    <div  className={style.modal} style={styleDisplay}>
        <Button type='close' text = {"X"} handleClick={() => dispatch(createCloseCartModal())}/>
        {children}
        <Button type='secondary' text = "Close" handleClick={() => dispatch(createCloseCartModal())}/>
    </div>
    )
}