import React from 'react';


const buttonTypes = {
    primary: 'btn-primary',
    secondary: "btn-secondary",
    close: 'btn-light ',
    danger: "btn-danger",
    warning: "btn-warning"
}


const Button = ({text, type, handleClick}) => {
    return (
        <button onClick={() =>handleClick()} className={`btn ${buttonTypes[type]}`}>
            {text}
        </button>
    )
}


export default Button;