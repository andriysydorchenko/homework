import React from 'react';
import {Provider} from "react-redux"
import './App.css';
import { ProductApp } from './client/ProductApp/components/ProductApp';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Modal} from "./shared/components/Modal";
import Navbar from "./client/Navbar/components/Navbar";
import {ProductCart} from "./client/ProductApp/components/ProductCart";
import store from "./store/store";

function App() {
  return (
      <Provider store={store}>
            <div className="App">
              <Navbar/>
              <Modal>
                  <ProductCart/>
              </Modal>
                <ProductApp/>
            </div>
      </Provider>
  );
}

export default App;
