import React from 'react';
import Button from "../../../../shared/components/Button/Button";
import { useDispatch} from "react-redux";
import {createOpenCartModal} from "../../../../store/creators/createOpenCartModal";
import {createClearCart} from "../../../../store/creators/createClearCart";

const Navbar = () => {

    const dispatch = useDispatch();
    const openCartModal = ()=> {
        dispatch(createOpenCartModal());
    }
    const clearCart = () => {
        dispatch(createClearCart());
    }
    return (
        <div className="d-flex justify-content-center">
            <Button type="warning" text= "Open cart" handleClick={openCartModal}/>
            <Button type="danger" text= "Clear cart" handleClick={clearCart}/>
        </div>
    );
};

export default Navbar;
