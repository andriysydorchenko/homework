import React from 'react'
import {createAddToCart} from "../../../../store/creators/createAddToCart";
import {useSelector, useDispatch, shallowEqual} from "react-redux";

export const ProductsList = () => {
    const state = useSelector(state => state, shallowEqual);
    const dispatch = useDispatch();

    const productsElements = state.productsListProps.map(product =>{
       const {title, price, src} = product;
       return (
        < div className = "card-block" >
            <h4 className = "card-title" > {title} </h4>
            <p className="card-text">Price: ${price.toFixed(2)}</p>
            <img className="img-fluid" src={src} alt={title}/>
            <a href="#"  className="add-to-cart btn btn-primary" onClick={() => dispatch(createAddToCart(product))}>Add to cart</a>
        </div>
       )
    })

    return (
        <ul className="d-flex">
            <h2>Products:</h2>
            {productsElements}
        </ul>
    )
}