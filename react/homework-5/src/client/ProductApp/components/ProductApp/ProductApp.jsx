import React  from 'react';
import {ProductsList} from '../ProductsList';


export const ProductApp = () => {

    return (
        <div className="container">
            <ProductsList
            />
        </div>
    )
}