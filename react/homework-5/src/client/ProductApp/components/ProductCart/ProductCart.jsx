import React from 'react'
import {useSelector, useDispatch, shallowEqual} from "react-redux";
import {createIncrementCount} from "../../../../store/creators/createIncementCount";
import {createDecrementCount} from "../../../../store/creators/createDecrementCount";
import {createRemoveFromCart} from "../../../../store/creators/createRemoveFromCart";

export const ProductCart = () => {
    const cart = useSelector(state => state.cart, shallowEqual);
    const dispatch = useDispatch();
    let totalPrice = 0;


    const cartElements = cart.map((element, index) =>{
        const price = element.count*element.product.price;
        totalPrice += price;
        // totalPrice = Math.round(totalPrice*100)/100;
        return ( <li className="row mb-2 d-flex">
            <span  className="col-20">{element.product.title} </span>
            <div className="col d-flex justify-content-center">
                <button
                    className="btn btn-outline-dark mr-1"
                    onClick={() => dispatch(createIncrementCount(element.product.id))}>+</button>
                <span className="mr-1">Count: {element.count} </span>
                <button
                    className="btn btn-outline-dark mr-1"
                    onClick={() => dispatch(createDecrementCount(element.product.id))}>-</button>
                <span className="mr-1">Price: ${price.toFixed(2)} </span>
                <button
                    className="btn btn-outline-danger mr-1"
                    onClick={() => dispatch(createRemoveFromCart(element.product.id))}>Delete</button>
            </div>
        </li>)
    }

    )
    
    return (
        <ul className="d-flex flex-column">
            <h2>Выбранное</h2>
            {cartElements}
            <hr/>
            <h3>Total price: ${totalPrice.toFixed(2)}</h3>
        </ul>
    )
}