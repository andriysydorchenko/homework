// const localCartList = JSON.parse(window.localStorage.getItem("cart"));

// export const cart = localCartList || [];
export const cart = [];

export const productsListProps =
    [
        {
            title: "Orange",
            price: 0.5,
            src: "http://www.azspagirls.com/files/2010/09/orange.jpg",
            id: 1
        },
        {
            title: "Banana",
            price: 1.22,
            src: "http://images.all-free-download.com/images/graphicthumb/vector_illustration_of_ripe_bananas_567893.jpg",
            id: 2
        },
        {
            title: "Lemon",
            price: 5,
            src: "https://3.imimg.com/data3/IC/JO/MY-9839190/organic-lemon-250x250.jpg",
            id: 3
        },
    ];

export const isOpenCartModal = false;