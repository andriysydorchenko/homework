import {cart as initialState} from '../intialState';
import {ADD_TO_CART, REMOVE_FROM_CART, CLEAR_CART, INCREMENT_COUNT, DECREMENT_COUNT} from "../action";

const deleteCartElement = (state, id)=>{
    const newCartList = [...state];
    const index = newCartList.findIndex( ({product}) => product.id === id);
    newCartList.splice(index, 1)
    window.localStorage.setItem("cart", JSON.stringify(newCartList));
    return newCartList
}

const changeCount = (state, id, increment = true) => {
    const newCartList = [...state];
    const index = newCartList.findIndex( ({product}) => product.id === id);
    if (increment) {
        newCartList[index] = {
            product: newCartList[index].product,
            count: newCartList[index].count + 1
        };
        window.localStorage.setItem("cart", JSON.stringify(newCartList));
        return newCartList
    }else {
        if (newCartList[index].count - 1 === 0)
            return deleteCartElement(state, id);
        else{
            newCartList[index] = {
                product: newCartList[index].product,
                count: newCartList[index].count - 1
            };
            window.localStorage.setItem("cart", JSON.stringify(newCartList));

            return newCartList
        }
    }
}

const addToCart = (state, cartElement) =>{
    const index = state.findIndex( ({product: {id}}) => id === cartElement.id);
    if (index === -1) {
        const newCartElement = {
            product: cartElement,
            count: 1
        }

        const newCartList  = [...state, newCartElement];
        window.localStorage.setItem("cart", JSON.stringify(newCartList));
        return newCartList
    } else return changeCount(state, cartElement.id);
}

export const cartReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case ADD_TO_CART:
            return addToCart(state, payload)

        case REMOVE_FROM_CART:
            return deleteCartElement(state, payload)

        case CLEAR_CART:
            return [];

        case INCREMENT_COUNT:
            return changeCount(state, payload)

        case DECREMENT_COUNT:
            return changeCount(state, payload, false)


        default:
            return state;
    }
}