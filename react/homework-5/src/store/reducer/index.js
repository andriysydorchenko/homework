import {cartReducer} from "./cartReducer";
import {modalReducer} from './modalReducer';
import {combineReducers} from 'redux';
import {productListReducer} from "./productListReducer";



export const rootReducer = combineReducers({
    cart: cartReducer,
    isOpenCartModal: modalReducer,
    productsListProps: productListReducer
});