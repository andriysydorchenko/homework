import {OPEN_CART_MODAL, CLOSE_CART_MODAL} from '../action';
import {isOpenCartModal as initialState} from '../intialState';

export const modalReducer = (state = initialState, {type, payload}) => {
    switch(type) {
        case OPEN_CART_MODAL:
            return true
        case CLOSE_CART_MODAL:
            return false
        default:
            return state;
    }
}