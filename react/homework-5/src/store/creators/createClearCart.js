import {CLEAR_CART} from "../action";

export const createClearCart = ()=>{
    return {
        type: CLEAR_CART,
    }
}