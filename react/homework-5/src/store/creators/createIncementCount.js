import {INCREMENT_COUNT} from "../action";

export const createIncrementCount = (payload) =>{
    return {
        type: INCREMENT_COUNT,
        payload
    }
}