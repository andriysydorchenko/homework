import {ADD_TO_CART} from "../action";

export const createAddToCart = (payload) =>{
    return {
        type: ADD_TO_CART,
        payload
    }
}