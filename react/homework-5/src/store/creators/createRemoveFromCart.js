import {REMOVE_FROM_CART} from "../action";

export const createRemoveFromCart = (payload) =>{
    return {
        type: REMOVE_FROM_CART,
        payload
    }
}