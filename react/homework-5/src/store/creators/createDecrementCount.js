import {DECREMENT_COUNT} from "../action";

export const createDecrementCount = (payload) =>{
    return {
        type: DECREMENT_COUNT,
        payload
    }
}