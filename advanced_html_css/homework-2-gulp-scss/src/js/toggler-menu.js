const toggler = document.querySelector("#navbar-toggler");
const target = toggler.dataset.target;
toggler.addEventListener("click", ()=>{
    toggler.classList.toggle("toggle");
    const elem = document.querySelector(target);
    elem.classList.toggle("toggle");
});
