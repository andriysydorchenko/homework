class Hamburger {

    static SIZE_SMALL={
        name:"SMALL",
        price:50,
        calories:20
    };
    static SIZE_LARGE={
        name:"BIG",
        price:100,
        calories:20
    };
    static STUFFING_CHEESE = {
        name:"CHEESE",
        price:10,
        calories:20
    };
    static STUFFING_SALAD ={
        name:"SALAD",
        price:20,
        calories:5
    };
    static STUFFING_POTATO = {
        name:"POTATO",
        price:15,
        calories:10
    };
    static TOPPING_MAYO={
        name:"MAYO",
        price:20,
        calories:5
    };
    static TOPPING_SPICE = {
        name:"SPICE",
        price:15,
        calories:0
    };
 constructor(size, stuffing) {
         //verification size
         if(!size){
             throw new HamburgerException('no size given');
         }
         if(size === Hamburger.SIZE_SMALL ||  size === Hamburger.SIZE_LARGE){
             this.size = size;
         } else {
             throw new HamburgerException('invalid size');
         }
         //verification stuffing
         if(!stuffing){
             throw new HamburgerException('no stuffing given');
         }
         if(stuffing === Hamburger.STUFFING_CHEESE ||  stuffing === Hamburger.STUFFING_SALAD || stuffing === Hamburger.STUFFING_POTATO){
             this.stuffing = size;
         } else {
             throw new HamburgerException('invalid stuffing');
         }

     this.topping = [];
 }

 addTopping(topping){
         if(!topping){
             throw new HamburgerException('no topping given');
         }
         if(topping === Hamburger.TOPPING_MAYO ||  topping === Hamburger.TOPPING_SPICE){
             if (!this.topping.includes(topping)){
                 this.topping.push(topping);
             } else {
                 throw new HamburgerException('duplicate topping');
               }
         }
         else {
             throw new HamburgerException('invalid topping');
         }
 };

 removeTopping(topping){

         if(topping === undefined){
             throw new HamburgerException('no topping given');
         }
         if(topping === Hamburger.TOPPING_MAYO ||  topping === Hamburger.TOPPING_SPICE){
             const indexToping = this.topping.indexOf(topping);
             if (indexToping !== -1){
                 this.topping.splice(indexToping, 1);
             } else {
                 throw new HamburgerException('there is no such topping');
             }
         }
         else {
             throw new HamburgerException('invalid topping');
         }
 };

 getToppings(){
     return this.topping;
 };

 getSize(){
     return  this.size;
 };

 getStuffing(){
     return  this.stuffing;
 };

 calculatePrice(){
     let priceToppings = 0;
     this.topping.forEach((item)=>priceToppings += item.price);
     const priceHamburger = this.size.price + this.stuffing.price + priceToppings;
     return priceHamburger;
 };

 calculateCalories(){
     let caloriesToppings = 0;
     this.topping.forEach((item)=>caloriesToppings += item.calories);
     const caloriesHamburger = this.size.calories + this.stuffing.calories + caloriesToppings;
     return caloriesHamburger;
 };

}


//Function constructor of error what happen while constructing hamburger
function HamburgerException(errorMassage) {
    this.message = `HamburgerException: ${errorMassage}`;
    this.stack = (new Error()).stack;
}
HamburgerException.prototype = Object.create(Error.prototype);
HamburgerException.prototype.name = "HamburgerException";


try{
    // маленький гамбургер с начинкой из сыра
    const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_MAYO);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1

// не передали обязательные параметры
    const h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
    const h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
    const h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'


}
catch (error) {
    console.log(error.message);
}

