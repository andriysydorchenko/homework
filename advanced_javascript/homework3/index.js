class Game {
    constructor(lvl){
            this.lvl = lvl;
            this.playerScores = 0;
            this.computerScores = 0;
    }
    startGame(){
        const field = new GameField();


        field
    }
    IncScorePlayer(){
        const textScorePlayer = document.querySelector("#score-player");
        this.playerScores++;
        textScorePlayer.textContent = this.playerScores;
    }
    IncScoreComp(){
        const textScoreComp = document.querySelector("#score-computer");
        this.computerScores++;
        textScoreComp.textContent = this.computerScores;
    }
    getRandomCell(){
        const index = Math.floor(Math.random() * maxCount);
        let cell = cells[index];
        if (cell.classList.contains("active")){
            cell = GameField.getRandomCell(cells, maxCount);
        }
        return cell;
    }

    setActiveCell(){
        const countCells = this.fieldHeight*this.fieldWidth;
        const cells = this.field.querySelectorAll(".cell");
        let cell = GameField.getRandomCell(cells, countCells);
        cell.classList.add("active");
        setTimeout(function(){cell.classList.remove("active")}, this.lvl);
    }

}

class GameField {
    static EASY_LVL = 1500;
    static MIDDLE_LVL = 1000;
    static HARD_LVL = 500;
    static SCORE_PLAYER = 0;
    static SCORE_COMPUTER = 0;
    static SMALL_FIELD = {
        fieldHeight: 3,
        fieldWidth: 3
    };
    static MIDDLE_FIELD = {
        fieldHeight: 6,
        fieldWidth: 6
    };
    static LARGE_FIELD = {
        fieldHeight: 10,
        fieldWidth: 10
    };
    static START_INTERVAL = null;
    constructor({fieldSize, id, classListCell, lvl}){
        this.fieldHeight = fieldSize.fieldHeight;
        this.fieldWidth = fieldSize.fieldWidth;
        this.id = id;
        // this.classList = classList;
        this.classListCell = classListCell;
        this.lvl = lvl;
        this.field = null;
        this.rows = [];

    }
    render(){
        this.field = document.createElement("table");
        const {field} = this;
        field.id = this.id;
        // elem.classList = this.classList.join(" ");
        const rows = [];
        for(let i =0; i <= this.fieldHeight; i++){
            rows[i] = this.renderRow();
        }
        field.append(...rows);
        return field;
    }
    renderRow(){
        const row = document.createElement("tr");
        const cells = [];
        for(let i =0; i <= this.fieldWidth; i++){
            cells[i] = this.renderCell();
        }
        row.append(...cells);
        return row;
    }
    renderCell(){
        const cell = document.createElement("td");
        cell.classList = this.classListCell.join(" ");
        cell.addEventListener("click", ()=>{
            if (cell.classList.contains("active")){
                cell.classList.remove("active");
                cell.classList.add("win");
                GameField.IncScorePlayer();
                GameField.stopGame(this.fieldWidth*this.fieldHeight);
            }else{
                cell.classList.add("losing");
                GameField.IncScoreComp();
                GameField.stopGame(this.fieldWidth*this.fieldHeight);
            }
        });
        return cell;
    }
    static IncScorePlayer(){
        const textScorePlayer = document.querySelector("#score-player");
        this.SCORE_PLAYER++;
        textScorePlayer.textContent = this.SCORE_PLAYER;
    }

    static IncScoreComp(){
        const textScoreComp = document.querySelector("#score-computer");
        this.SCORE_COMPUTER++;
        textScoreComp.textContent = this.SCORE_COMPUTER;
    }

    static getRandomCell(cells, maxCount){
        const index = Math.floor(Math.random() * maxCount);
        let cell = cells[index];
        if (cell.classList.contains("active")){
            cell = GameField.getRandomCell(cells, maxCount);
        }
        return cell;
    }

    setActiveCell(){
        const countCells = this.fieldHeight*this.fieldWidth;
        const cells = this.field.querySelectorAll(".cell");
        let cell = GameField.getRandomCell(cells, countCells);
        cell.classList.add("active");
        setTimeout(function(){cell.classList.remove("active")}, this.lvl);
    }

    static startGame(field){
        this.SCORE_PLAYER = 0;
        this.SCORE_COMPUTER = 0;
        this.START_INTERVAL = setInterval(field.setActiveCell.bind(field),  field.lvl);
    }

    static stopGame(maxCount){
        if (this.SCORE_COMPUTER >= maxCount/2 || this.SCORE_PLAYER >= maxCount/2) {
            alert("computer win");
            clearInterval(this.START_INTERVAL);
        }
    }

}

const table = {
    fieldSize: GameField.LARGE_FIELD,
    id: "gameField",
    classListCell: ["cell"],
    lvl: GameField.EASY_LVL
};


const divBtnLvl = document.querySelector(".lvl");
divBtnLvl.addEventListener("click", event=>{
    const btnLvl = event.target;
    const btnsLvl= divBtnLvl.children;
    for(let i = 0; i < btnsLvl.length; i++){
        btnsLvl[i].classList.remove("active");
    }
    btnLvl.classList.add("active");
    if (btnLvl.id === "easy-level"){
        table.lvl = GameField.EASY_LVL;
    } else if (btnLvl.id === "middle-level"){
        table.lvl = GameField.MIDDLE_LVL;
    } else if (btnLvl.id === "hard-level"){
        table.lvl = GameField.HARD_LVL;
        console.log(table.lvl);
    }
})




const btnStart = document.querySelector("#start-game");
btnStart.addEventListener("click", ()=>{
    const fieldGame = new GameField(table);
    const divGame = document.querySelector('#game');
    divGame.append(fieldGame.render());
    GameField.startGame(fieldGame);
});




