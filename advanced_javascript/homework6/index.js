class Post {
    static PostId = [];
    constructor(title, author, email, textPost, id, userId ){
        this.title = title;
        this.author = author;
        this.email = email;
        this.textPost = textPost;
        this.id = id;
        this.userId = userId;
        this.post = null
    }
    render(){
        if (!this.post) {
            this.post= document.createElement("div");
            this.post.className = "post";
        }
        this.post.innerHTML = `
            <h3 class = "post-title">${this.title}</h3>
            <span class = "post-author">Author: ${this.author},</span>
            <a href = "mailto" class="post-email">Email: ${this.email}</a>
            <p class = "post-text">${this.textPost}</p>
            <button id = "post-change">Change Post</button>
            <button id = "post-delete">Delete Post</button>
        `;
        const btnChangePost = this.post.querySelector("#post-change");
        btnChangePost.addEventListener("click", this.editPost.bind(this));
        const btnDeletePost = this.post.querySelector("#post-delete");
        btnDeletePost.addEventListener("click", this.deletePost.bind(this));
        return this.post
    }
    editPost(){
        this.post.innerHTML = `
            <input type="text" class="post-title" value = '${this.title}' placeholder="Enter title post">
            <input type="text" class="post-author" value = '${this.author}' placeholder="Enter author name post">
            <input type="email" class="post-email" value = '${this.email}' placeholder="Enter email of author post">
            <textarea class="post-text" placeholder="Enter text post">${this.textPost}</textarea>
            <button id="post-accept">Accept</button>
        `;

        const btnChangePost = this.post.querySelector("#post-accept");
        btnChangePost.addEventListener("click", ()=>{
            const title = this.post.querySelector(".post-title").value;
            const author = this.post.querySelector(".post-author").value;
            const email = this.post.querySelector(".post-email").value;
            const textPost = this.post.querySelector(".post-text").value;


            const body = {
                userId: this.userId,
                id: this.id,
                title,
                body: textPost
            };

            const request = new XMLHttpRequest();
            request.open('PUT', `https://jsonplaceholder.typicode.com/posts/${this.id}`);
            request.setRequestHeader('Content-Type', "application/json");
            request.responseType = 'json';
            request.send(JSON.stringify(body));
            request.onload = function() {
                if (request.status >= 300) {
                    console.log(`Ошибка ${request.status}: ${request.statusText}`);
                } else {
                    console.log("put request was done");
                    this.title = title;
                    this.author = author;
                    this.email = email;
                    this.textPost = textPost;
                    this.render();
                    console.log(request.response);
                }
            };
            request.onerror = function() {
                alert("Запрос не удался");
            };
        });


        return  this.post;
    }
    static createPost(container){
        const newPost = document.createElement("form");
        newPost.className = "post";
        container.prepend(newPost);

        newPost.innerHTML =  `
            <input type="text" class="post-title" value = '' placeholder="Enter title post">
            <input type="text" class="post-author" value = '' placeholder="Enter author name post">
            <input type="email" class="post-email" value = '' placeholder="Enter email of author post">
            <textarea class="post-text" placeholder="Enter text post"></textarea>
            <input type="submit" id="post-create" value="Create">
        `;
        newPost.addEventListener("submit", (e)=>{
            e.preventDefault();
            const title = newPost.querySelector(".post-title").value;
            const author = newPost.querySelector(".post-author").value;
            const email = newPost.querySelector(".post-email").value;
            const textPost = newPost.querySelector(".post-text").value;
            Post.PostId.sort(function(a, b) {
                return a - b;
            });
            const id = Post.PostId[Post.PostId.length-1] + 1;
            newPost.remove();
            const newPostObj= new Post(title, author, email, textPost, id, "1",);
            container.prepend(newPostObj.render());

            // REQUEST POST
            const body ={
                userId: 1,
                id: newPostObj.id,
                title: newPostObj.title,
                body: newPostObj.textPost,
            };
            const request = new XMLHttpRequest();
            request.open('POST', `https://jsonplaceholder.typicode.com/posts`);
            request.setRequestHeader('Content-Type', "application/json");
            request.responseType = 'json';
            request.send(JSON.stringify(body));
            request.onload = function() {
                if (request.status >= 300) {
                    console.log(`Ошибка ${request.status}: ${request.statusText}`);
                } else {
                    console.log("create new post");
                    console.log(request.response);
                }
            };
            request.onerror = function() {
                alert("Запрос не удался");
            };
        });
    }
    deletePost(){
        this.post.remove();
        const request = new XMLHttpRequest();
        request.open('DELETE', `https://jsonplaceholder.typicode.com/posts/${this.id}`);
        request.setRequestHeader('Content-Type', "application/json");
        request.responseType = 'json';
        request.send();
        request.onload = function() {
            if (request.status >= 300) {
                console.log(`Ошибка ${request.status}: ${request.statusText}`);
            } else {
                console.log("delete post");
                console.log(request.response);
            }
        };
        request.onerror = function() {
            alert("Запрос не удался");
        };
    }
}


const request = new XMLHttpRequest();
const request1 = new XMLHttpRequest();
request.open('GET', "https://jsonplaceholder.typicode.com/users");
request.responseType = 'json';
request1.open('GET', "https://jsonplaceholder.typicode.com/posts");
request1.responseType = 'json';
request.send();
const container = document.getElementById("container");

request.onload = function() {
    if (request.status >= 300) {
        console.log(`Ошибка ${request.status}: ${request.statusText}`);
    } else {
        // console.log(request.response);
            const arrayUser = request.response;
            request1.send();
            request1.onload = function () {
                if (request.status >= 300) {
                    console.log(`Ошибка ${request.status}: ${request.statusText}`);
                } else {
                    const arrayPost = request1.response;
                    arrayUser.forEach( user =>{
                        const id = user.id;
                        arrayPost.forEach( post => {
                            if (post.userId === id){
                                const newPost  = new Post(post.title ,user.name,user.email, post.body, post.id, post.userId);
                                Post.PostId.push(post.id);
                                container.prepend(newPost.render());
                            }
                        })
                    })
                }
            }
        // const resultArr = request.response.split(". ");
        // console.log(resultArr)
    }
};
request.onerror = function() {
    alert("Запрос не удался");
};

const btnCreatePost = document.querySelector("#create-post");
btnCreatePost.addEventListener("click", (event)=>{
    event.preventDefault();
    Post.createPost(container);
});
