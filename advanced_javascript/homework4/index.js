
class DropList {
   static DragElem= null;
   constructor({classname, title: {titleClassname},  caseList: {caseListClassname, caseListId}, btn: {bntClassname, btnId} }){
       this.classname = classname;
       this.title = {titleClassname};
       this.caseList = {caseListClassname, caseListId, caseListDom: null};
       this.btn = {bntClassname, btnId, btnDom: null};
       this.dropContainer = null;
       
   }
   render(){
       this.dropContainer  = document.createElement("div");
       this.dropContainer.className = this.classname;
       this.dropContainer.insertAdjacentHTML("afterbegin", `
                <h2 class = ${this.title.titleClassname}>Case List</h2>
                `);

       this.caseList.caseListDom = document.createElement("ul");
       const {caseListDom} =  this.caseList;
       caseListDom.className = this.caseList.caseListClassname;
       caseListDom.id = this.caseList.caseListId;
       this.dropContainer.append(caseListDom);

       this.btn.btnDom = document.createElement("button");
       const {btnDom} = this.btn;
       btnDom.className = this.btn.bntClassname;
       btnDom.id = this.btn.btnId;
       btnDom.innerText = "Add case...";
       btnDom.addEventListener("click", this.createCase.bind(this));
       this.dropContainer.append(btnDom);

       caseListDom.addEventListener("dragover", this.dragOver);
       caseListDom.addEventListener("drop", this.drop);

       return this.dropContainer;
   }
   createCase(){
       const caseProporty = {
           classname: "case-item",
           title: {
               titleClassname: "case-title",
               titleId: "case-title",
           },
           text: {
               textClassname: "case-text",
               textId: "case-text"
           }
       };
       const caseItem = new DropCase(caseProporty);
       this.caseList.caseListDom.append(caseItem.render());
   }

    static DragStart(e){
        DropList.DragElem = this;
        console.log(DropList.DragElem);
    }

    dragOver(e){
        e.preventDefault();
    }

    drop(e){
        const dragElem = DropList.DragElem;
        if(e.target === this) {
            this.append(dragElem);
        }
        else {
            console.log(e.target.classList.contains("case-item"));
            if(e.target.classList.contains("case-item")){
                e.target.before(dragElem);
            }
        }
        DropList.DragElem = null;
    }


}




class DropCase{
    constructor({classname, title:{titleClassname, titleId}, text: {textClassname, textId}}){
        this.classname = classname;
        this.title = {
            titleClassname, 
            titleId
        };
        this.text = {
            textClassname,
            textId,
        };
        this.caseDom = null;
    }
    render(){
        this.caseDom = document.createElement("li");
        const {caseDom} = this;
        caseDom.className = this.classname;
        caseDom.draggable = "true";
        caseDom.insertAdjacentHTML("afterbegin", `
            <h3 class = ${this.title.titleClassname} id = ${this.title.titleId}>Title</h3>
            <p class = ${this.text.textClassname} id = ${this.text.textId}>case text</p>
        `);
        

        this.caseDom.addEventListener("dblclick", event =>{
            const edibleText = event.target;
            if (edibleText.classList.contains(this.text.textClassname)) {
                const textArea = document.createElement("textarea");
                textArea.id = this.text.textId;
                textArea.className = this.text.textClassname;
                textArea.placeholder = "Enter case text";
                edibleText.replaceWith(textArea);
            }
            else if(edibleText.classList.contains(this.title.titleClassname)){
                const textArea = document.createElement("textarea");
                textArea.id = this.title.titleId;
                textArea.className = this.title.titleClassname;
                textArea.placeholder = "Enter case title";
                edibleText.replaceWith(textArea);
            }
        });

        this.caseDom.addEventListener("change", event =>{
            const edibleTextArea = event.target;
            if (edibleTextArea.id === this.text.textId){
                const innerText = edibleTextArea.value;
                const edibleText = document.createElement("p");
                edibleText.id = this.text.textId;
                edibleText.className = this.text.textClassname;
                edibleText.textContent = innerText;
                edibleTextArea.replaceWith(edibleText);
            }
            if (edibleTextArea.id === this.title.titleId){
                const innerText = edibleTextArea.value;
                const edibleText = document.createElement("h3");
                edibleText.id = this.title.titleId;
                edibleText.className = this.title.titleClassname;
                edibleText.textContent = innerText;
                edibleTextArea.replaceWith(edibleText);
            }
        });

        caseDom.addEventListener("dragstart", DropList.DragStart);
        return caseDom;

    }
    
}




const list = {
    classname: "drop-container",
    title: {
        titleClassname: "drop-header"
    },
    caseList: {
        caseListClassname: "case-list",
        caseListId: "case-list"
    },
    btn: {
        bntClassname:"add-case",
        btnId: "add-case"
    }
};
const body  = document.querySelector("body");
const addColumn = document.querySelector(".btn-add-column");
const filter = document.querySelector("#filterAlphabet");
addColumn.addEventListener("click", (event) =>{
    event.preventDefault();
    const newList = new DropList(list);
    body.append(newList.render());
});
filter.addEventListener("click", (event) =>{
    event.preventDefault();
    const cases = [...(document.querySelectorAll(".case-item"))];

    // Array.prototype.sort.call(cases, sortfunc);
    cases.sort(sortfunc);
    const column = document.querySelector(".case-list");
    column.innerHTML = "";
    cases.forEach( item => {
        column.insertAdjacentElement("beforeend", item);
    })

});

function sortfunc(a, b){
    const titleA = a.querySelector("#case-title").innerText;
    const titleB = b.querySelector("#case-title").innerText;
    if (titleA > titleB) {
        return 1;
    }
    if (titleA < titleB) {
        return -1;
    }
    return 0;
}